// 封装axios
import axios from 'axios'
// 通过axios的create方法创建一个axios的实例
const instance = axios.create({
  baseURL: 'http://toutiao.itheima.net',
  timeout: 5000
})

export default instance
